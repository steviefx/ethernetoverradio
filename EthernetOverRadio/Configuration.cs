﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EthernetOverRadio
{
    public static class Configuration
    {
        public static bool SingleChannel { get; set; }
        public const string sniffer = "/home/pi/433Utils/RPi_utils/RFSniffer";
        public const string transmitter = "/home/pi/433Utils/RPi_utils/codesend";
        public const string workingdir = "/home/pi/433Utils/RPi_utils/";

    }
}
