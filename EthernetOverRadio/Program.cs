﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace EthernetOverRadio
{
    class Program
    {
        public static List<byte[]> sent = new List<byte[]>();
        public static List<int> received = new List<int>();
        public static List<string> receivedstring = new List<string>();
        public static bool endoftrans = false;

        static void Main(string[] args)
        {
            if(args.Length == 0)
            {
                Console.WriteLine("EOR started without appropriate arguments.\r\n" +
                    "Valid arguments:\r\n" +
                    "-s \tSingle Channel - Allows the raspberry to pickup messages sent by itself\r\n" +
                    "-d \tDual Channel   - Disables the pickup of messages sent by itself");
                return;
            } else if(args.Length > 1)
            {
                Console.WriteLine("Too many arguments - aborting");
            } else
            {
                if(args[0] == "-s")
                {
                    Configuration.SingleChannel = true;
                } else if(args[0] == "-d")
                {
                    Configuration.SingleChannel = false;
                } else
                {
                    Console.WriteLine("Unkown Arguments - aborting");
                    Environment.Exit(-1);
                }
            }
            new Thread(() =>
            {
                Sniff();
            }).Start();
            while (true)
            {
                Console.WriteLine("Enter data to send - ASCII only:");
                string str = Console.ReadLine();
                Send(str);
            }
        }

        private static void Sniff()
        {
            ProcessStartInfo psi = new ProcessStartInfo()
            {
                FileName = Configuration.sniffer,
                RedirectStandardError = true,
                RedirectStandardInput = true,
                RedirectStandardOutput = true,
                UseShellExecute = false,
            };
            Process snifferproc = new Process();
            snifferproc.StartInfo = psi;
            Dictionary<int, char> currenttransmission = new Dictionary<int, char>();
            snifferproc.OutputDataReceived += DataHandler;
            snifferproc.Start();
            snifferproc.BeginOutputReadLine();
            while (true)
            {
                Thread.Sleep(2000);
            }
        }

        private static void DataHandler(object sendingProcess, DataReceivedEventArgs outLine)
        {
            if (received.Contains(int.MaxValue))
            {
                return;
            }
            foreach (var b in sent)
            {
                if(BitConverter.ToInt32(b) == Convert.ToInt32(outLine.Data))
                {
                    // Our Sent-List contains the data we just sent - ignore.
                    return;
                }
            }
            if (received.Contains(Convert.ToInt32(outLine.Data)))
            {
                return;
            }
            else
            {
                received.Add(Convert.ToInt32(outLine.Data));
            }
            string rawoutput = outLine.Data;
            byte[] rawbytes = BitConverter.GetBytes(Convert.ToInt32(rawoutput));
            //if the random string is "11111111" = end of transmission
            if (rawbytes[2] == BitConverter.GetBytes(255)[0])
            {
                if(receivedstring.Count > 0)
                {
                    receivedstring.Add(System.Environment.NewLine);
                }
                foreach (string s in receivedstring)
                {
                    Console.Write(s);
                }
                received.Clear();
                receivedstring.Clear();
            }
            else
            {
                receivedstring.Add(Encoding.ASCII.GetString(new byte[] { rawbytes[0] }));
            }
        }

        private static void Send(string str)
        {
            ProcessStartInfo psi = new ProcessStartInfo()
            {
                FileName = Configuration.transmitter,
                RedirectStandardOutput = true,
                RedirectStandardInput = true,
                RedirectStandardError = true,
                UseShellExecute = false,
                WorkingDirectory = Configuration.workingdir
            };
            int seqnumber = 0;
            string input = str;
            
            foreach (char c in input.ToCharArray())
            {
                List<byte[]> tosend = new List<byte[]>();
                Random rnd = new Random();
                int randomnumber = rnd.Next(0, 254);
                                          // the first byte here is padding so we can convert to int32
                tosend.Add(new byte[] { new byte(), BitConverter.GetBytes(randomnumber)[0], new byte(), new byte() });
                // two byte padding here
                tosend.Add(new byte[] { new byte(), new byte(), BitConverter.GetBytes(seqnumber)[0], new byte() });
                // three byte padding here
                tosend.Add(new byte[] { new byte(), new byte(), new byte(), Encoding.ASCII.GetBytes(c.ToString())[0] });
                int argument = 0;
                foreach (var b in tosend)
                {
                    argument += BitConverter.ToInt32(b);
                }
                var arr = BitConverter.GetBytes(argument);
                Array.Reverse(arr);
                if (!Configuration.SingleChannel)
                {
                    sent.Add(arr);
                }
                string finalargument = BitConverter.ToInt32(arr, 0).ToString();
                psi.Arguments = finalargument;
                Process.Start(psi);
                seqnumber++;
                Thread.Sleep(500);
            }
            //End of transmission, send new byte[] where byte[][1] == 11111111
            ProcessStartInfo endoftrans = new ProcessStartInfo()
            {
                FileName = Configuration.transmitter,
                Arguments = "16711680",
                WorkingDirectory = Configuration.workingdir,
                RedirectStandardError = true,
                RedirectStandardInput = true,
                RedirectStandardOutput = true
            };

            Process.Start(endoftrans);
            sent.Clear();
            Thread.Sleep(1000);
        }
    }
}
