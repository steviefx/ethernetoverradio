# What is Ethernet Over Radio? 

Ethernet Over Radio allows the transmission of data via 433 MHz devices for Raspberry Pi.
It has been developed with .NET Core 2.1.

## How to get it running

A script has already been provided, which will install all necessary components for EOR to work.

Download the file here:

https://gitlab.com/steviefx/ethernetoverradio/blob/master/InstallScript.sh

and run it with the following command:

```
sudo bash InstallScript.sh
```

The script will install the dotnet application to ~/dotnet and EOR to ~/app.

## Dependencies

The following dependencies will be installed, if not already installed (list may be incomplete):

curl 
libunwind8 
gettext
Dotnet Core Framework
git
433Utils
wiringpi


If you do not trust the file here - for whatever reason - you may of course install all dependencies by yourself.
Just download the file and execute the commands manually.

## How to use the application

EOR in its current state can only send and receive Data manually.
Depending on your setup, you have to decide if you want to run the application in Single-Channel mode or in Dual-Channel mode.

Single-Channel mode should be used when there's only one Raspberry Pi with Radio modules in use, while Dual-Channel is to be used when there's two Raspberry Pi's in use.
Starting in Single-Channel mode will display messages sent by itself in the terminal, while Dual-Channel only displays messages which have been sent by another Raspberry. 

To start in Single-Channel mode, start the application like this: 

```
dotnet EthernetOverRadio.dll -s
```

To start in Dual-Channel mode, use the argument -d instead of -s:

```
dotnet EthernetOverRadio.dll -d
```

## Known errors

If your version of 433Utils does not compile, use the provided 433Utils folder in this git repo. The code has been corrected and should compile.